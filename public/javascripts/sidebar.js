console.log('TEST');
$(function () {
  console.log('loaded sidebar.js');

  $('.side-bar .nav .nav-item a').click(function (e) {
    e.preventDefault();
    let id_selector = $(this)[0].hash;
    let list = $('.side-bar .nav');
    // console.log($(this), 'Click');
    // console.log(id_selector, 'all svsdelector');
    // console.log($('.side-bar .nav'), 'Item id');
    //close all the tab windows
    $('.side-bar .nav .nav-item a.active').each(function () {

      $(this).removeClass('active');
    });
    //open the selected tab window
    $(id_selector).addClass('active');
  });

});