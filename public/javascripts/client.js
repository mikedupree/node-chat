$(function () {
  var socket = io();
  var User = [];

  $('form').submit(function () {
    socket.emit('chat message', $('#chat-message-input').val());
    $('#chat-message-input').val('');
    return false;
  });

  socket.on('user', function (msg) {
    $('#username').append($('<a href="#" data-toggle="tooltip" title="Hooray!">' + msg + '</a>'));
  });

  /**
   *
   */
  socket.on('the user logged in', function (user) {
    //TODO this needs cleaning the user var shows to much info
    console.log("update var User");
    User = user;
    console.log(User);
  });

  socket.on('chat message', function (msg) {
    console.log(msg);

    var body = msg["body"];
    var message_recipient = msg["to"];
    var author = msg["from"];
    var timeStamp = Math.floor(Date.now());
    var time = new Date();
    time = time.toLocaleString('en-US', {hour: 'numeric', minute: 'numeric', hour12: true});

    $('#messages').append($('<tr class="msg-' + +timeStamp + '">'));
    //add the picture, msg, timestamp
    $('#messages .msg-' + timeStamp).append($('<td class="" width="72">').append($('<img src="http://via.placeholder.com/50x50?text=' + author[0] + author[1] + '"/>')));
    $('#messages .msg-' + timeStamp).append($('<td class="">').text(author + ": " + body));
    $('#messages .msg-' + timeStamp).append($('<td class="time" width="92"><i class="fa fa-clock-o"></i>').text(time));
  });

  var url = window.location.href;
  var name = 'user';
  console.log(url, 'url');
  if (!url) url = window.location.href;
  name = name.replace(/[\[\]]/g, "\\$&");
  var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
    results = regex.exec(url);
  if (!results) console.log('NULL');
  if (!results[2]) console.log('empty');
  console.log( decodeURIComponent(results[2].replace(/\+/g, " ")));
  // Send the message to the server
  //@TODO finish
  // socket.emit("private-message", {
  //   "username": $(this).find("input:first").val(),
  //   "content": $(this).find("textarea").val()
  // });


  socket.on('update user list', function (list) {
    console.log(list);
    var username = 'mdupree'; //@todo get username so we can highlight there account
    var timeStamp = Math.floor(Date.now());
    var time = new Date();

    //Rebuild the user list
    $('#user-list').remove();
    $('#contact-list').append($('<div id="user-list">'));
    //Add the new contacts
    for (var i = 0; i < list.length; i++) {
      //Append the username media body + username
      $('#user-list').append(
        $('<a href="?user=' + list[i].username + '" class="chatperson">').append(
          $('<div class="namechat">').append(
            $('<div class="pname">').text(list[i].username)
          )
        )
      );
//          $('#user-list .media-body.bd-' + i).append($('<p class="media-heading" id="contactName">').text(list[i].username));
    }
  });
});