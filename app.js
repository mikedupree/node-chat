var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var bodyParser = require('body-parser');
var sassMiddleware = require('node-sass-middleware');
var mongoose = require('mongoose');
var session = require('cookie-session')({ secret: 'secret' });
// var MongoStore = require('connect-mongo')(session);
var app = express();
var index = require('./routes/index');
var users = require('./routes/users');
var chat = require('./routes/chat');

//connect to MongoDB
mongoose.connect('mongodb://localhost/playground');
var db = mongoose.connection;

//handle mongo error
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
    // we're connected!
    console.log('We are connected to mongo');
});

// var mongoStore = new MongoStore({
//   mongooseConnection: db
// });

//use sessions for tracking login
// app.use(session({
//     key: 'express.sid',
//     secret: 'work hard',
//     resave: true,
//     saveUninitialized: false,
//     store: mongoStore
// }));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'twig');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(session);
app.use(sassMiddleware({
  src: path.join(__dirname, 'public'),
  dest: path.join(__dirname, 'public'),
  indentedSyntax: true, // true = .sass and false = .scss
  sourceMap: true
}));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', index);
app.use('/users', users);
app.use('/chat', chat);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});


// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

//export app and session middleware
// var moduleExport = [app, session, express, mongoStore, cookieParse];
var moduleExport = [app, session, express];

module.exports = moduleExport;
