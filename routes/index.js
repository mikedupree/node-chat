var express = require('express');
var router = express.Router();
var User = require('../models/user');
var bcrypt = require('bcrypt');

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', {title: '[DupeChat]'});
});

/* POST home page. */
router.post('/', function (req, res, next) {
  console.log('INCOMING:');
  console.log(req.body);

  /**
   * User Create Post
   * - check all fields filled then create the user
   */
  if (req.body.email && req.body.username && req.body.password && req.body.passwordConf) {
    console.log('[User Create POST]');

    // confirm that user typed same password twice
    if (req.body.password !== req.body.passwordConf) {
      var err = new Error('Passwords do not match.');
      err.status = 400;
      return next('/?error=bad_password');
    }


    //create temp user object
    var userData = {
      email: req.body.email,
      username: req.body.username,
      password: req.body.password,
      passwordConf: req.body.passwordConf,
      isOnline: true,
      socketId: "none"
    };

    bcrypt.hash(userData.password, 10, function (err, hash) {
      if (err) {
        return next(err);
      }
      userData.password = hash;
      //use models.create to insert data into the db
      User.create(userData, function (err, user) {
        if (err) {
          return next(err)
        } else {
          //create the session
          req.session.userId = user._id;
          //remove sensitive information here
          var tempUser = user;
          //TODO clean up this password hiding
          tempUser["password"] = "!@#$%^&*()";
          tempUser["tempPassword"] = "!@#$%^&*()";
          req.session.user = tempUser;
          return res.redirect('/chat');
        }
      });
    });


  }
  /**
   * User Login Post
   * - check login email and password combo exist
   */
  else if (req.body.email && req.body.password) {
    console.log('[User Login POST]');
    console.log('Request  SocketId:: ' + req.socketId);
    User.authenticate(req.body.email, req.body.password, function (error, user) {
      if (error) {
        return next(err);
      } else if(!user) {
        var err = new Error('Wrong email or password.');
        err.status = 401;
        return next(err);
      } else {
        user.isOnline = true;
        user.save();
        console.log('User ID = ' + user._id);
        req.session.userId = user._id;
        req.session.user = user;
        return res.redirect('/chat');
      }
    });
  } else {
    // user did not fill out all the field
    console.log('[ERROR DURING POST]');
    var err = new Error('All fields must be filled out');
    err.status = 400;
    return next(err);
  }
});

// GET for logout logout
router.get('/logout', function (req, res, next) {
  if (req.session) {
    User.findOne({email: req.session.user.email})
      .exec(function (err, user) {
        //handle any errors
        if (err) {
          return callback(err);
        }
        console.log(user.email + ' field isOnline set to false');
        user.isOnline = false;
        user.save();
      });

    // delete session object
    req.session.destroy(function (err) {
      if (err) {
        return next(err);
      } else {
        //redirect them to the home page
        return res.redirect('/');
      }
    });
  }
});
module.exports = router;
