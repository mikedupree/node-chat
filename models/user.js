var mongoose = require('mongoose');
var bcrypt   = require('bcrypt');

/**
 * Define the User entity models
 */
var UserSchema = new mongoose.Schema({
    email: {
        type: String,
        unique: true,
        required: true,
        trim: true
    },
    username: {
        type: String,
        unique: true,
        required: true,
        trim: true
    },
    password: {
        type: String,
        required: true
    },
    passwordConf: {
        type: String,
        required: true
    },
    isOnline: {
        type: Boolean,
        required: true
    },
    socketId: {
      type: String,
      unique: false,
      required: true,
      default: "none"
    }
});

/**
 * Authenticate User credentials
 *
 * @param email
 * @param password
 * @param callback
 */
UserSchema.statics.authenticate = function (email, password, callback) {
    User.findOne({email: email})
        .exec(function(err, user){
        //handle any errors
        if(err){
            return callback(err);
        }
        //check if user not found
        else if(!user){
            err = new Error('User not found');
            err.status = 401;
            console.log('[ERROR: USER NOT FOUND!]');
            return callback(err);
        }
        //compare password hash in db against one sent
        bcrypt.compare(password, user.password, function (err, result) {
            console.log(result);
            if(result === true) {
                return callback(null, user);
            } else {
              console.log('[ERROR: PASSWORD COMPARE FAILED!]');
              return callback();
            }
        });

    });

};

/**
 * Schema Pre-save function
 * Hash passwords before saving them to the db
 */
UserSchema.pre('create', function (next) {
  var user = this;
  console.log('{create}');
  console.log('user ' + user.email);
  bcrypt.hash(user.password, 10, function (err, hash) {
        if (err) {
            return next(err);
        }
        user.password = hash;
        user.save();
        next();
    });
});


var User = mongoose.model('User', UserSchema);
module.exports = User;