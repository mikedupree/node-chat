class Room {

  constructor() {
    this.id = Math.random();//TODO make a proper id scheme
    this._name = "Room::" + this.id;
    this.visitors = 0;
    this.users = [];
  }
  set name(name) {
    this._name = name.charAt(0).toUpperCase() + name.slice(1);
  }
  get name() {
    return this._name;
  }
  enterRoom( user ) {
    console.log('Hello, ' + user + ' has entered room' + this._name);
    if(user && user.username)
      this.users[user.username] = user;
  }
}